Quick Start Guide
-----------------

### For servers with fully-qualified domain names

On a recent Debian or Ubuntu server with a fully-qualified domain name (FQDN), run the following commands:
<!-- These quick start instructions are also on https://gitlab.com/aegir/www.aegirproject.org/blob/master/config.yml -->

```sh
# Debian 10 (Buster), Ubuntu 18.04 and later:
sudo apt install mysql-server
sudo mysql_secure_installation
sudo wget -O /usr/share/keyrings/aegir-archive-keyring.gpg https://debian.aegirproject.org/aegir-archive-keyring.gpg
echo "deb [signed-by=/usr/share/keyrings/aegir-archive-keyring.gpg] https://debian.aegirproject.org stable main" | sudo tee -a /etc/apt/sources.list.d/aegir-stable.list
sudo apt update
sudo apt install aegir3 aegir-archive-keyring
```

### For servers without fully-qualified domain names

To get a Aegir instance for trial purposes on a temporary VM (either local or remote) without an FQDN, follow the [Aegir Development VM](https://gitlab.com/aegir/aegir-dev-vm) instructions.

### For more information

See the [Installation Guide](/install) for more details, or guidance on installing on other operating systems.
